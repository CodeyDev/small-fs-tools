#!/bin/bash



if [ "$1" == "-h" -o "$1" == "--help" ]; then
    cat <<USAGE
usage: fseventsdblock [path to volume]      Disables .fseventsd on a volume
       fseventsdblock -h|--help    Print this message
USAGE
    exit 0
fi

volumepath="${1:-/Volumes/CIRCUTPY}"

rm -rf "$volumepath/.fseventsd"


touch "$volumepath/.fseventsd"


chmod a=r "$volumepath/.fseventsd"
