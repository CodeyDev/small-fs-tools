#!/bin/bash

if [ "$1" == "-h" -o "$1" == "--help" ]; then
    cat <<USAGE
usage: defseventsdblock [path to volume]      Re-Enables the .fseventsd folder on a volume
       defseventsdblock -h|--help    Print this message
USAGE
    exit 0
fi


volumepath="${1:-/Volumes/CIRCUTPY}"

chmod a=rw "$volumepath/.fseventsd"


rm -rf "$volumepath/.fseventsd"
