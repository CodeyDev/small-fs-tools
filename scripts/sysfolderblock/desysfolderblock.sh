#!/bin/bash

if [ "$1" == "-h" -o "$1" == "--help" ]; then
    cat <<USAGE
usage: desysfolderblock [path to volume]      Re-Enables .Trashes and .fseventsd on a volume
       desysfolderblock -h|--help    Print this message
USAGE
    exit 0
fi


volumepath="${1:-/Volumes/CIRCUTPY}"

chmod a=rw "$volumepath/.Trashes"
chmod a=rw "$volumepath/.fseventsd"

rm -rf "$volumepath/.Trashes"
rm -rf "$volumepath/.fseventsd"