#!/bin/bash



if [ "$1" == "-h" -o "$1" == "--help" ]; then
    cat <<USAGE
usage: sysfolderblock [path to volume]      Disables .Trashes and .fseventsd on a volume
       sysfolderblock -h|--help    Print this message
USAGE
    exit 0
fi

volumepath="${1:-/Volumes/CIRCUTPY}"

rm -rf "$volumepath/.Trashes"
rm -rf "$volumepath/.fseventsd"

touch "$volumepath/.Trashes"
touch "$volumepath/.fseventsd"

chmod a=r "$volumepath/.Trashes"
chmod a=r "$volumepath/.fseventsd"