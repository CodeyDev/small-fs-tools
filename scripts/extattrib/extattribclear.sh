#!/bin/bash



if [ "$1" == "-h" -o "$1" == "--help" ]; then
    cat <<USAGE
usage: extattribclear [Path to volume]      Clears The ._ files on a volume. BE CAREFUL WITH THIS
       extattribclear -h|--help    Print this message
USAGE
    exit 0
fi

volumepath="${1:-/Volumes/CIRCUTPY}"

rm -rf "$volumepath/._*"
