#!/bin/bash



if [ "$1" == "-h" -o "$1" == "--help" ]; then
    cat <<USAGE
usage: trashblock [path to volume]      Disables .Trashes on a volume
       trashblock -h|--help    Print this message
USAGE
    exit 0
fi

volumepath="${1:-/Volumes/CIRCUTPY}"

rm -rf "$volumepath/.Trashes"


touch "$volumepath/.Trashes"


chmod a=r "$volumepath/.Trashes"
