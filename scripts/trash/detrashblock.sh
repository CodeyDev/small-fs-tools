#!/bin/bash

if [ "$1" == "-h" -o "$1" == "--help" ]; then
    cat <<USAGE
usage: detrashblock [path to volume]      Re-Enables .Trashes on a volume
       detrashblock -h|--help    Print this message
USAGE
    exit 0
fi


volumepath="${1:-/Volumes/CIRCUTPY}"

chmod a=rw "$volumepath/.Trashes"


rm -rf "$volumepath/.Trashes"
