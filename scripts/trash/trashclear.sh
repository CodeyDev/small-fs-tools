#!/bin/bash



if [ "$1" == "-h" -o "$1" == "--help" ]; then
    cat <<USAGE
usage: trashclear [Path to volume]      Clears The .Trashes on a volume
       trashclear -h|--help    Print this message
USAGE
    exit 0
fi

volumepath="${1:-/Volumes/CIRCUTPY}"

rm -rf "$volumepath/.Trashes"
